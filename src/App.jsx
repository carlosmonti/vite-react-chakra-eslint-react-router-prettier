import { LoginButton, LogoutButton, Profile } from "./components";
import { Route, Routes } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import { HomePage } from "./pages";
import { Flex } from "@chakra-ui/react";

function App() {
  // eslint-disable-next-line no-unused-vars
  const { user, isAuthenticated, isLoading } = useAuth0();

  return (
    <Flex className="app">
      <Routes>
        <Route path="/" element={<HomePage />} />
      </Routes>
    </Flex>
    // <div className="card">
    //   {isAuthenticated ? <LogoutButton /> : <LoginButton />}
    //   {isAuthenticated && <Profile />}
    // </div>
  );
}

export default App;
